﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace RespellerProKeygen
{
    class Program
    {
        static void Main(string[] args)
        {
            const string decryptLicenseText = "ReSpellerPro3License";
            Console.WriteLine("Process Started");
            var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, DateTime.Now.Ticks + ".lic");
            File.WriteAllText(filePath, EncryptString(decryptLicenseText));
            Console.WriteLine($"license saved successfully in path: {filePath}");
            //System.Diagnostics.Process.Start("explorer.exe", AppDomain.CurrentDomain.BaseDirectory);
            Console.ReadKey();
        }

        private static string EncryptString(string decryptText)
        {
            Console.WriteLine("Generating initializing vector");
            var bytes = Encoding.ASCII.GetBytes("lgvfsnntfwbhqmif");

            Console.WriteLine("Generating key vector");
            var rgbKey = Encoding.ASCII.GetBytes("btvangoqsbfknzeveqmvfiifbmichiyz");

            Console.WriteLine("Decrypting Key");
            var decryptBytes = Encoding.UTF8.GetBytes(decryptText);
            var stream = new MemoryStream();
            var algorithm = SymmetricAlgorithm.Create();
            var stream2 = new CryptoStream(stream, algorithm.CreateEncryptor(rgbKey, bytes), CryptoStreamMode.Write);
            stream2.Write(decryptBytes, 0, decryptBytes.Length);
            stream2.Close();

            return Convert.ToBase64String(stream.ToArray());
        }
    }
}
